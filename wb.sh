#!/bin/bash 
ifconfig $1 down
iwconfig $1 mode Managed
macchanger -p $1
sudo ifconfig $1 up 
systemctl restart NetworkManager.service
