#!/bin/bash
# paclist - creates list of all installed packages
# reinstall with pacman -S $(cat pkglist)

pacman -Qqet | grep -v "$(pacman -Qqg base)" | grep -v "$(pacman -Qqm)" >  ~/Desktop/pkglist

# A list of local packages (includes AUR and locally installed)
pacman -Qm > ~/Desktop/pkglocallist
