#!/usr/bin/env python3
import os
import sys

try:
    pwd = sys.argv[1]
except:
    pwd = os.getcwd()
dir1 = pwd + "/raw"
dir2 = pwd + "/converted"

if not os.path.exists(dir1):
    os.makedirs(dir1)
if not os.path.exists(dir2):
    os.makedirs(dir2)
# print(os.path.splitext(pwd+"/ffmpeg.py")[0])

for i in os.listdir(pwd):
    if i != "ffmpeg.py" and i != "raw" and i != "converted":
        # print(str(os.path.splitext(i)[0]))
        os.system("ffmpeg -i \"" + pwd + "/" + i + "\" -c:v libx265 -y \"" + pwd + "/converted/" + str(
            os.path.splitext(i)[0]) + ".mkv\"")
        os.rename(i, pwd + "/raw/" + i)
# input()
