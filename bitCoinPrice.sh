#!/bin/bash 

OCP=$(curl -s "https://digiarz.com/webservice/api/")
OCP2=$(curl -s "https://api.coinmarketcap.com/v1/ticker/")
echo $OCP | jq '.time' -r 


echo ""
echo "Bitcoin"
echo "Price USD:" $(echo $OCP | jq '.BTC.rates.USD.rate' -r)
echo "Price IRT:" $(echo $OCP | jq '.BTC.rates.TMN.rate' -r)

echo ""
echo "Bitcoin Cash"
BCH=$(echo $OCP2 | jq '.[2].price_usd' -r)
echo "Price USD:" $BCH
echo "Price IRT:" 

echo ""
echo "Ethereum"
echo "Price USD:" $(echo $OCP | jq '.ETH.rates.USD.rate' -r)
echo "Price IRT:" $(echo $OCP | jq '.ETH.rates.TMN.rate' -r)

echo ""
echo "Ethereum classic"
echo "Price USD:" $(echo $OCP | jq '.ETC.rates.USD.rate' -r)
echo "Price IRT:" $(echo $OCP | jq '.ETC.rates.TMN.rate' -r)

echo ""
echo "Monero"
echo "Price USD:" $(echo $OCP | jq '.XMR.rates.USD.rate' -r)
echo "Price IRT:" $(echo $OCP | jq '.XMR.rates.TMN.rate' -r)

echo ""
echo "Ripple"
echo "Price USD:" $(echo $OCP | jq '.XRP.rates.USD.rate' -r)
echo "Price IRT:" $(echo $OCP | jq '.XRP.rates.TMN.rate' -r)

echo ""
echo "Z-cash"
echo "Price USD:" $(echo $OCP | jq '.ZEC.rates.USD.rate' -r)
echo "Price IRT:" $(echo $OCP | jq '.ZEC.rates.TMN.rate' -r)

echo ""
echo "Factom"
echo "Price USD:" $(echo $OCP | jq '.FCT.rates.USD.rate' -r)
echo "Price IRT:" $(echo $OCP | jq '.FCT.rates.TMN.rate' -r)

echo ""
echo "Litecoin"
echo "Price USD:" $(echo $OCP | jq '.LTC.rates.USD.rate' -r)
echo "Price IRT:" $(echo $OCP | jq '.LTC.rates.TMN.rate' -r)

echo ""
echo "Dogecoin"
echo "Price USD:" $(echo $OCP | jq '.DOGE.rates.USD.rate' -r)
echo "Price IRT:" $(echo $OCP | jq '.DOGE.rates.TMN.rate' -r)

echo ""
echo "Digital cash"
echo "Price USD:" $(echo $OCP | jq '.DASH.rates.USD.rate' -r)
echo "Price IRT:" $(echo $OCP | jq '.DASH.rates.TMN.rate' -r)








