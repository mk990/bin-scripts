#!/bin/bash

cat <<EOM> /etc/systemd/system/multi-user.target.wants/tor.service
# This service is actually a systemd target,
# but we are using a service since targets cannot be reloaded.

[Unit]
Description=Anonymizing overlay network for TCP (multi-instance-master)

[Service]
User=debian-tor
Type=simple
RemainAfterExit=yes
ExecStart=/usr/bin/tor -f /etc/tor/torrc
ExecReload=/usr/bin/kill -HUP $MAINPID
KillSignal=SIGINT
LimitNOFILE=8192
PrivateDevices=yas

[Install]
WantedBy=multi-user.target
EOM

systemctl daemon-reload
